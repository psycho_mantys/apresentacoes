FROM alpine/git AS build

WORKDIR /build

COPY . .

RUN sh -c 'set +e +o pipefail ; git submodule init 2>&1 ; git pull --no-rebase --verbose --recurse-submodules 2>&1 || exit 0' && \
  apk add --no-cache bash && \
  ./gen.sh && \
  find . -iname ".git" | xargs -n1 rm -rf

FROM node:16-alpine

WORKDIR /app

COPY --from=build /build /app/

RUN apk add --no-cache curl && npm install reveal-multiplex

CMD node node_modules/reveal-multiplex

HEALTHCHECK CMD curl --fail http://127.0.0.1:1948 || exit 1

