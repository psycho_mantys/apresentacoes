#!/bin/bash

read BASE EXPOENTE

RESULTADO=1
for (( i=1 ; i<=EXPOENTE ; i++ )) ; do
	RESULTADO="$(( RESULTADO*BASE ))"
done

echo "${RESULTADO}"

