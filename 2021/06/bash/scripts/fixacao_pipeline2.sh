#!/bin/bash

echo 'Digite o limite de faltas:'
read limite_faltas

cat entrada.txt | \
	while read falta nome ; do
		if (( limite_faltas<=falta )) ; then
			echo "${falta} ${nome}"
		fi
	done

