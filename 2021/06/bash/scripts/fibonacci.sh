#!/bin/bash

read TERMO

PRIMEIRO=0
SEGUNDO=1

for (( i=0 ; i<TERMO ; i++ )) ; do
	echo "${PRIMEIRO}"
	temp="$(( PRIMEIRO+SEGUNDO ))"
	PRIMEIRO="${SEGUNDO}"
	SEGUNDO="${temp}"
done

