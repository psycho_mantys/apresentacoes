#!/bin/bash

maior_nota=-1
maior_nome=

while read nota nome ; do
	if (( maior_nota<nota )) ; then
		maior_nota="${nota}"
		maior_nome="${nome}"
	fi
done < entrada.txt

echo "${maior_nota} ${maior_nome}"

