#!/bin/bash

OFS="${IFS}"
IFS=';'

while read col1 col2 col3 ; do
	echo "${col1}"
	echo "${col2}"
	echo "${col3}"
done <<<'col1;col2;col3'

IFS="${OFS}"

