#!/bin/bash

if [ "$1" ] ; then
	cd "$1" || exit 1
fi
set - .
root_directory_count_slash=$(pwd | tr '/' '\n' | wc -l)

base_dir=http://localhost/static/directory-listing/

declare -a entry_txt=('TXT' 'txt' "icons/text.svg")
declare -a entry_dir=('DIR' 'dir' "icons/folder.svg")
declare -a entry_unknown=('UKN' 'ukn' "icons/unknown.svg")
declare -a entry_css=('CSS' 'css' "icons/css3.svg")
declare -a entry_js=(' JS' 'js' "icons/js.svg")
declare -a entry_img=('IMG' 'img' "icons/img.svg")
declare -a entry_pdf=('PDF' 'pdf' "icons/pdf.svg")
declare -a entry_py=(' PY' 'py' "icons/py.svg")
declare -a entry_html=('HTM' 'html' "icons/html5.svg")
declare -a entry_script=('SCR' 'script' "icons/script.svg")
declare -a entry_sql=('SQL' 'sql' "icons/sql.svg")
declare -a entry_audio=('AUD' 'audio' "icons/audio.svg")
declare -a entry_video=('AUD' 'video' "icons/video.svg")
declare -a entry_info=('INF' 'info' "icons/info.svg")
declare -a entry_archive=('ARV' 'archive' "icons/archive.svg")
declare -a entry_code=('COD' 'code' "icons/code.svg")
declare -a entry_doc=('DOC' 'doc' "icons/doc.svg")
declare -a entry_presentation=('PPT' 'presentation' "icons/presentation.svg")
declare -a entry_spreadsheet=('SPS' 'spreadsheet' "icons/spreadsheet.svg")
declare -a entry_font=('FNT' 'font' "icons/font.svg")

get_entry_type(){
	case "${1}" in
		*txt)
			echo txt
			;;
		*css)
			echo css
			;;
		*js)
			echo js
			;;
		*jpg | *gif | *svg | *jpeg | *png | *bmp)
			echo img
			;;
		*pdf)
			echo pdf
			;;
		*py)
			echo py
			;;
		*html)
			echo html
			;;
		*.sh | *.bash | *ksh | *.bsh)
			echo script
			;;
		*sql)
			echo sql
			;;
		*mp3 | *wav | *ogg | *mpa | *m4a)
			echo audio
			;;
		*mp4 | *rmvb | *wmv | *.avi | *.flv)
			echo video
			;;
		*.inf | *info)
			echo info
			;;
		*zip | *.tar.* | *rar | *.gz | *.tgz | *xz)
			echo archive
			;;
		*conf | *.cnf | *Dockerfile* | *.tex | *.sty | *Makefile* | .git*)
			echo code
			;;
		*.odt | *doc | *docx)
			echo doc
			;;
		*.ods | *.xls | *.xlsx | *.csv)
			echo spreadsheet
			;;
		*.odp | *.ppt | *.pptx)
			echo code
			;;
		*.eot | *.ttf | *.woff)
			echo font
			;;
		*)
			echo unknown
			;;
	esac
}

get_entry(){
	eval "echo \${entry_$1[$2]}"
}

mkindex(){
# usage: auto-index [dir]
	local directory=

	if [ ! "$1" ] ; then
		directory=.
	else
		directory="$1"
	fi

	(
	cd "${directory}"

	slash_count=$(pwd | tr '/' '\n' | wc -l)
	slash_count=$(( slash_count-root_directory_count_slash ))

	if [ "${slash_count}" = 0 ] ; then
		base_dir=./static/directory-listing
	else
		base_dir=$( eval printf "../%0.s" "{1..$slash_count}" )/static/directory-listing
	fi

	cat <<-EOF
		<!DOCTYPE html>
		<html>
		<head>
			<title>Index of ${directory}</title>
			<link rel="stylesheet" href="${base_dir}/grid-darkmode.css"/>
			<meta name="viewport" content="width=device-width, initial-scale=1"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		</head>
		<body>
			<main>
			<h1 id="title">Index of <span class="js-path"></span></h1>
			<table id=indexlist>
				<tr class="indexhead">
					<th class="indexcolicon">
						<img src="${base_dir}/icons/blank.svg" alt="[ICO]">
					</th>
					<th class="indexcolname">
						<a href="?C=N;O=D">Name</a>
					</th>
					<th class="indexcolsize">
						<a href="?C=S;O=A">Size</a>
					</th>
				</tr>
				<tr class="even-parentdir">
					<td class="indexcolicon">
						<a href="../ls_index.html">
							<img src="${base_dir}/icons/folder-home.svg" alt="[PARENTDIR]">
						</a>
					</td>
					<td class="indexcolname">
						<a href="../ls_index.html">Parent Directory</a>
					</td>
					<td class="indexcollastmod">&nbsp;</td>
					<td class="indexcolsize"> - </td>
				</tr>
EOF

	entry_num="odd"
	for x in * ; do
		#INDEX=`ls -1 $1 | sed "s/^.*/      <li\>\<a\ href=\"&\"\>&\<\\/a\>\<\\/li\>/"`
		if [ "$x" != "ls_index.html" ] ; then
			entry_type="$( get_entry_type "${x}" )"
			entry_link="${x}"
			entry_name="${x}"
			if [ -d "${x}" ] ; then
				entry_type="dir"
				entry_link="${x}/ls_index.html"
				entry_name="${x}/"
			fi

			cat<<-EOF
				<tr class="${entry_num}-${entry_type}">
					<td class="indexcolicon">
						<a href="${entry_link}">
							<img src="${base_dir}/$(get_entry "${entry_type}" 2)" alt="[$(get_entry "${entry_type}" 0)]">
						</a>
					</td>
					<td class="indexcolname">
						<a href="${entry_link}">${entry_name}</a>
					</td><td class="indexcollastmod">0000-00-00 00:00  </td>
					<td class="indexcolsize">  - </td>
				</tr>
EOF
			if [ "${entry_num}" = "odd" ] ; then
				entry_num='even'
			else
				entry_num='odd'
			fi
		fi
	done
	cat<<-EOF
	</table>
	</main>

	<footer>
		<em>Psycho Mantys</em>
	</footer>

	<script>
		(function(){
			function joinUntil(array, index, separator) {
				var result = [];

				for (var i = 0; i <= index; i++) {
					result.push(array[i]);
				}

				return result.join(separator);
			}

			// Make all the icon links unfocusable with tab
			var iconLinks = document.querySelectorAll('.indexcolicon a');
			Array.prototype.forEach.call(iconLinks, function(link) {
				link.setAttribute('tabindex', '-1');
			});

			var path = document.querySelector('.js-path');
			var pathParts = location.pathname.split('/').slice(0, -1);

			// Removing empty strings
			for(var i=0; i<pathParts.length; ){
				if(pathParts[i]){
					i++;
				}else{
					pathParts.splice(i, 1);
				}
			}

			var pathContents = ['<a href="/ls_index.html">/</a>'];
			Array.prototype.forEach.call(pathParts, function(part, index) {
				pathContents.push('<a href="/' + joinUntil(pathParts, index, '/') + '/ls_index.html">' + decodeURI(part) + '</a>');
			});

			path.innerHTML = pathContents.join('&rsaquo;');
		})();
	</script>
</body>
</html>
EOF
	)
}

ndx=ls_index.html
find "." -type d | grep -v -e '/reveal.js' -e '/.git' | while read -r dir ; do
(
	mkindex "${dir}" > "${dir}/${ndx}"
)
done

# vim: nu ts=4 noet ft=bash:
